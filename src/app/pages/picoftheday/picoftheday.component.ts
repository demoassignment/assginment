import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/service/web.service';
import { UseSubjectService } from 'src/app/service/use-subject.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-picoftheday',
  templateUrl: './picoftheday.component.html',
  styleUrls: ['./picoftheday.component.scss']
})

/**
 * this component only shows the bootstrap grids and use of subject
 */

export class PicofthedayComponent implements OnInit {
  pickOfTheDay: object = {};
  constructor(public ws: WebService, private subjectService: UseSubjectService) {
    // subscribed subject
    this.subjectService.getImage().subscribe((image: any) => {
      this.pickOfTheDay = image.data;
    });
  }
  ngOnInit() {
    this.getPicofTheDay();
  }

  getPicofTheDay(): void {
    this.ws.getPicofTheDay().pipe(
      map(result => {
        return result.contextWrites.to;
      })
    ).subscribe(res => {
      // feed data to subject, it just an example of subject.
      this.subjectService.setImage(res);
    });
  }


}
