import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { PicofthedayComponent } from './picoftheday.component';
import { HttpClientModule } from '@angular/common/http';
import { WebService } from 'src/app/service/web.service';

describe('PicofthedayComponent', () => {
  let component: PicofthedayComponent;
  let fixture: ComponentFixture<PicofthedayComponent>;
  let testService: WebService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PicofthedayComponent],
      imports: [HttpClientModule],
      providers: [WebService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicofthedayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    testService = TestBed.get(WebService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // checking getPicofTheDay method
  it('getPicofTheDay should there', () => {
    expect(component.getPicofTheDay).toBeTruthy();
  });

  // checking webservice is ijected or not
  it('Webservice must be injected', () => {
    inject([WebService], (injectService: WebService) => {
      expect(injectService).toBe(testService);
    });

  });

});
