import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/service/web.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-eonet-layers',
  templateUrl: './eonet-layers.component.html',
  styleUrls: ['./eonet-layers.component.scss']
})
export class EonetLayersComponent implements OnInit {

  layers: object = [];
  // defined column for ag-grid
  columnDefs = [
    { headerName: 'Name', field: 'name', sortable: true, filter: true },
    { headerName: 'Format', field: 'format' },
    { headerName: 'Service Type Id', field: 'serviceTypeId', sortable: true, filter: true },
    { headerName: 'ServiceUrl', field: 'serviceUrl', sortable: true, filter: true }
  ];

  constructor(private ws: WebService) { }

  ngOnInit() {
    this.getLayers();
  }

  getLayers() {
    // ws is instance if webservice
    this.ws.getLayers().pipe(
      map(result => {
        return result.contextWrites.to.categories[0].layers.map(obj => {
          obj.format = obj.parameters[0].FORMAT; // defined and assigned inner array value to outer field
          return obj;
        });
      })
    ).subscribe(res => {
      this.layers = res;
    }, error => {
      console.log(error);
    });
  }

  onGridReady(event): void {
    // fix table to available width
    event.api.sizeColumnsToFit();
  }
}
