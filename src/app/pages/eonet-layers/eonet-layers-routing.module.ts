import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EonetLayersComponent } from './eonet-layers.component';

const routes: Routes = [{ path: '', component: EonetLayersComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EonetLayersRoutingModule { }
