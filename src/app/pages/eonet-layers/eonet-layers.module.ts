import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EonetLayersRoutingModule } from './eonet-layers-routing.module';
import { EonetLayersComponent } from './eonet-layers.component';
import { AgGridModule } from 'ag-grid-angular';


@NgModule({
  declarations: [EonetLayersComponent],
  imports: [
    CommonModule,
    EonetLayersRoutingModule,
    AgGridModule
  ]
})
export class EonetLayersModule { }
