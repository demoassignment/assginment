import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EonetCategoriesComponent } from './eonet-categories.component';

const routes: Routes = [{ path: '', component: EonetCategoriesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EonetCategoriesRoutingModule { }
