import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EonetCategoriesRoutingModule } from './eonet-categories-routing.module';
import { EonetCategoriesComponent } from './eonet-categories.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [EonetCategoriesComponent],
  imports: [
    CommonModule,
    EonetCategoriesRoutingModule,
    NgbModule
  ]
})
export class EonetCategoriesModule { }
