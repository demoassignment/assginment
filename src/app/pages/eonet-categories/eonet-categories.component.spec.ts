import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EonetCategoriesComponent } from './eonet-categories.component';
import { HttpClientModule } from '@angular/common/http';

describe('EonetCategoriesComponent', () => {
  let component: EonetCategoriesComponent;
  let fixture: ComponentFixture<EonetCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EonetCategoriesComponent ],
      imports: [HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EonetCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getCategories should there', () => {
    expect(component.getCategories).toBeDefined();
  });
});
