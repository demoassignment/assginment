import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/service/web.service';
import { map } from 'rxjs/operators';
import { ConditionalExpr } from '@angular/compiler';

@Component({
  selector: 'app-eonet-categories',
  templateUrl: './eonet-categories.component.html',
  styleUrls: ['./eonet-categories.component.scss']
})
export class EonetCategoriesComponent implements OnInit {
  categories: any[];
  constructor(public ws: WebService) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(): void {
    this.ws.getCategories()
      .subscribe(res => {
        if (res.callback === 'success') {
          this.categories = res.contextWrites.to.categories;
        }
      }, error => {
        console.log(error);
      });
  }

}
