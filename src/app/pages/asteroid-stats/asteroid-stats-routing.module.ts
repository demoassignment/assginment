import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsteroidStatsComponent } from './asteroid-stats.component';

const routes: Routes = [{ path: '', component: AsteroidStatsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsteroidStatsRoutingModule { }
