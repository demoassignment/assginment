import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsteroidStatsRoutingModule } from './asteroid-stats-routing.module';
import { AsteroidStatsComponent } from './asteroid-stats.component';
import {DataTableModule} from 'angular-6-datatable';

@NgModule({
  declarations: [AsteroidStatsComponent],
  imports: [
    CommonModule,
    AsteroidStatsRoutingModule,
    DataTableModule
  ]
})
export class AsteroidStatsModule { }
