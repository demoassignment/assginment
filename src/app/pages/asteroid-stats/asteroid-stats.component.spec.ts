import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsteroidStatsComponent } from './asteroid-stats.component';
import { HttpClientModule } from '@angular/common/http';

describe('AsteroidStatsComponent', () => {
  let component: AsteroidStatsComponent;
  let fixture: ComponentFixture<AsteroidStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AsteroidStatsComponent],
      imports: [HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsteroidStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('getStats method should should be defined.', () => {
    expect(component.getStats).toBeDefined();
  });
});
