import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/service/web.service';

@Component({
  selector: 'app-asteroid-stats',
  templateUrl: './asteroid-stats.component.html',
  styleUrls: ['./asteroid-stats.component.scss']
})
export class AsteroidStatsComponent implements OnInit {
  stats: object = {};
  constructor(public ws: WebService) { }

  ngOnInit() {
    // loading the stats
    this.getStats();
  }

  getStats(): void {
    this.ws.getAsteroidStats().subscribe(res => {
      console.log(res);
      if (res.callback === 'success') {
        this.stats = res.contextWrites.to;
      } else {
        console.log('error', res);
      }
    }, error => {
      console.log(error);
    });
  }

}
