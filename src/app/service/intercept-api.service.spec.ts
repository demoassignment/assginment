import { TestBed } from '@angular/core/testing';

import { InterceptApiService } from './intercept-api.service';

describe('InterceptApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterceptApiService = TestBed.get(InterceptApiService);
    expect(service).toBeTruthy();
  });
});
