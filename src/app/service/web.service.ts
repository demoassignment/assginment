import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const apiURL = 'https://nasaapidimasv1.p.rapidapi.com';

@Injectable({
  providedIn: 'root'
})

export class WebService {

  constructor(private http: HttpClient) { }

  getPicofTheDay(): Observable<any> {
    // return this.http.post(apiURL + '/getPictureOfTheDay', {});  // if Rapid api works
    return this.http.get('./assets/mockedData/picoftheday.json');
  }

  getAsteroidStats(): Observable<any> {
    // return this.http.post(apiURL + '/getAsteroidStats', {});
    return this.http.get('./assets/mockedData/steroidStats.json');
  }

  getCategories(): Observable<any> {
    // return this.http.post(apiURL + '/getEONETCategories', {});
    return this.http.get('./assets/mockedData/categories.json');
  }

  getLayers(): Observable<any> {
    // return this.http.post(apiURL + '/getEONETLayers', {});
    return this.http.get('./assets/mockedData/layers.json');
  }

}
