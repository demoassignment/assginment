import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UseSubjectService {

  private subject = new Subject();
  constructor() { }

  setImage(imageData) {
    this.subject.next({ data: imageData });
  }

  getImage() {
    return this.subject.asObservable();
  }

}
