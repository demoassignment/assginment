import { TestBed } from '@angular/core/testing';

import { UseSubjectService } from './use-subject.service';

describe('UseSubjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UseSubjectService = TestBed.get(UseSubjectService);
    expect(service).toBeTruthy();
  });
});
