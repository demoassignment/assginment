import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'picoftheday', loadChildren: () => import('./pages/picoftheday/picoftheday.module').then(m => m.PicofthedayModule), },
  { path: 'asteroidStats', loadChildren: () => import('./pages/asteroid-stats/asteroid-stats.module').then(m => m.AsteroidStatsModule) },
  { path: 'eonetLayers', loadChildren: () => import('./pages/eonet-layers/eonet-layers.module').then(m => m.EonetLayersModule) },
  { path: 'eonetCategories', loadChildren: () => import('./pages/eonet-categories/eonet-categories.module').then(m => m.EonetCategoriesModule) },
  { path: '', redirectTo: 'picoftheday', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
